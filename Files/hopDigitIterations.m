% A script that tests a hopfield network for digit (0-9)detection & reconstruction, using different iterations and noise levels

% Error and iteration limits
clear all; close all; clc;

single = false

if single
    misclassificationError = hopdigit_v2(5.5,14); % Store error for current setup
else
    noiselevels = 0:0.5:40; % min:step:max
    num_iters = 0:25:100; num_iters(1) = 1;
    misclassificationErrors = zeros(length(noiselevels), length(num_iters)); % Stores misclassification errors
    
    for j = 1:length(num_iters)
        for i = 1:length(noiselevels)
            misclassificationErrors(i, j) = hopdigit_v2(noiselevels(i),num_iters(j)); % Store error for current setup
        end
    end
    
    misclassificationErrors = misclassificationErrors * 100;
    
    figure
    plot(misclassificationErrors)
    title('MSE vs Noise level: Hopfield net (digits 0-9)')
    legend( 'Iterations: ' + string(num_iters), 'Location', 'southeast')
    xlabel('Noise Level')
    ylabel('Error %')
    
    figure
    indices = 1:int16(length(noiselevels)/4):length(noiselevels);
    plot(misclassificationErrors(indices,:)')
    title('MSE vs Iterations: Hopfield net (digits 0-9)')
    legend('Noise: ' + string(noiselevels(indices)), 'Location', 'southeast')
    xlabel('Iterations * 10')
    ylabel('Error %')
    
    figure
    [X,Y] = meshgrid(noiselevels, num_iters);
    surf(X,Y,misclassificationErrors')
    title('MSE vs Noise & Iterations: Hopfield net (digits 0-9)')
    xlabel('Noise Level')
    ylabel('Iterations')
    zlabel('Error %')
    colorbar
end