% A script that tests a hopfield network for digit (0-9)detection & reconstruction, using different iterations and noise levels

% Error and iteration limits
clear all; close all; clc;

single = false

if single
    misclassificationError = hopdigit_v2(5.5,14); % Store error for current setup
else
    noiselevels = 0:0.5:40; % min:step:max
    num_iters = 0:25:100; num_iters(1) = 1;
    misclassificationErrors = zeros(length(noiselevels), length(num_iters)); % Stores misclassification errors
    
    for j = 1:length(num_iters)
        for i = 1:length(noiselevels)
            misclassificationErrors(i, j) = hopdigit_v2(noiselevels(i),num_iters(j)); % Store error for current setup
        end
    end
    
    misclassificationErrors = misclassificationErrors * 100;
    
    figure
    plot(misclassificationErrors)
    title('MSE vs Noise level: Hopfield net (digits 0-9)')
    legend( 'Iterations: ' + string(num_iters), 'Location', 'southeast')
    xlabel('Noise Level')
    ylabel('Error %')
    
    figure
    indices = 1:int16(length(noiselevels)/4):length(noiselevels);
    plot(misclassificationErrors(indices,:)')
    title('MSE vs Iterations: Hopfield net (digits 0-9)')
    legend('Noise: ' + string(noiselevels(indices)), 'Location', 'southeast')
    xlabel('Iterations * 10')
    ylabel('Error %')
    
    figure
    [X,Y] = meshgrid(noiselevels, num_iters);
    surf(X,Y,misclassificationErrors')
    title('MSE vs Noise & Iterations: Hopfield net (digits 0-9)')
    xlabel('Noise Level')
    ylabel('Iterations')
    zlabel('Error %')
    colorbar
end

%A script to test if 2d Hopfield network can recognize letter of alphabet

function misclassificationError = hopdigit_v2(noiselevel,num_iter)

verbose = false;

% close all

load digits; clear size
[N, dim]=size(X);
maxx=max(max(X));

%Values must be +1 or -1
X(X==0)=-1;
%-------------------------------------------------------------------------

%Attractors of the Hopfield network

zero = X(1,:); %to visualize: digit=reshape(X(1,:),15, 16)'; -> imshow(digit); 
one = X(21,:);
two = X(41,:);
three = X(61,:);
four = X(81,:);
five = X(101,:);
six = X(121,:);
seven = X(141,:);
eight = X(161,:);
nine = X(181,:);

index_dig = [1,21,41,61,81,101,121,141,161,181];
num_dig = 10;
%--------------------------------------------------------------------------

T = [zero;one;two;three;four;five;six;seven;eight;nine]';

%Create network
net = newhop(T);
net.trainParam.showWindow = 0;

%Check if digits are attractors
[Y,~,~] = sim(net,num_dig,[],T);
Y = Y';

if verbose 
    figure;

    subplot(num_dig,3,1);

    for i = 1:num_dig
    digit = Y(i,:);
    digit = reshape(digit,15,16)'; 
    subplot(num_dig,3,((i-1)*3)+1);
    imshow(digit)
    if i == 1
        title('Attractors')
    end
    hold on
    end
end


%The plots show that they are attractors.

%------------------------------------------------------------------------



% Add noise to the digit maps

noise = noiselevel*maxx; % sd for Gaussian noise

Xn = X; 
for i=1:N;
  Xn(i,:) = X(i,:) + noise*randn(1, dim);
end


% %Show noisy digits:

if verbose 
    subplot(num_dig,3,2);
    
    for i = 1:num_dig
    digit = Xn(index_dig(i),:);
    digit = reshape(digit,15,16)';
    subplot(num_dig,3,((i-1)*3)+2);
    imshow(digit)
    if i == 1 
        title(sprintf('Noisy (Lvl: %0.2f)', noiselevel))
    end
    hold on
    end
end

%------------------------------------------------------------------------

%See if the network can correct the corrupted digits 


num_steps = num_iter;

Xn = Xn';
Tn = {Xn(:,index_dig)};
[Yn,~,~] = sim(net,{num_dig num_steps},{},Tn);
Yn = Yn{1,num_steps};
Yn = Yn';

% Calculate misclassification error
misclassificationError = 0;
for i = 1:num_dig
    [~, predictedDigit] = min(mean((Y - Yn(i,:)).^2,2)); % Find the predicted digit with min MSE
    if i ~= predictedDigit
        misclassificationError = misclassificationError + 1; % Increase error occurence
    end
end

misclassificationError = misclassificationError / num_dig;

if verbose 
    subplot(num_dig,3,3);
    for i = 1:num_dig
    digit = Yn(i,:);
    digit = reshape(digit,15,16)';
    subplot(num_dig,3,((i-1)*3)+3);
    imshow(digit)
    if i == 1
        title(sprintf('Hopfield (%d%% error, %d steps)', misclassificationError * 100, num_iter))
    end
    hold on
    end
end